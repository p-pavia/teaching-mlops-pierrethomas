import spacy
import logging
from fastapi import FastAPI
from typing import Dict

app = FastAPI()

logging.info("Loading model..")
nlp = spacy.load("./models")

@app.get("/api/intent")
def intent_inference(sentence : str) -> Dict[str, float]:
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return inference.cats

@app.get("/api/intent-supported-languages")
def supported_languages() -> Dict[int, str]:
    return {0: "fr-FR"}

@app.get("/health", status_code=200)
def health_check_endpoint() -> None: pass
